package de.wsc.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class EasyProperties {
    private static final String PROPERTIES_FILENAME = "easy.properties";
    private static final String PROPERTIES_FILENAME_KEY = "easyfile";
    private static final Map<String, String> arguments = new HashMap<>();
    private static final Properties easyProperties = new Properties();

    private static void readProperties() {
        String filename = Objects.requireNonNullElse(getValue(PROPERTIES_FILENAME_KEY), PROPERTIES_FILENAME);
        File file = new File(filename);
        if (file.exists()) {
            try (FileInputStream stream = new FileInputStream(file)) {
                easyProperties.load(stream);
            } catch (IOException e) {
                // ignore
            }
        }
    }

    public static List<String> init(String... args) {
        List<String> arguments =  proceedArguments(args);
        readProperties();
        return arguments;
    }

    public static String getValue(String key) {
        String value = arguments.get(key);
        if (value != null) {
            return value.trim();
        }

        value = easyProperties.getProperty(key);
        if (value != null) {
            return value;
        }

        value = System.getProperty(key);
        if (value != null) {
            return value;
        }

        return System.getenv(key);
    }

    private static List<String> proceedArguments(String... args) {
        List<String> result = new ArrayList<>();
        for (String arg : args) {
            if (isKeyValue(arg)) {
                String[] keyValue = getKeyValue(arg);
                arguments.put(keyValue[0], keyValue[1]);
            } else {
                result.add(arg);
            }
        }
        return result;
    }

    static boolean isKeyValue(String arg) {
        if (!arg.startsWith("--")) {
            return false;
        }

        String keyValue = arg.substring(2);
        return keyValue.contains("=") || keyValue.contains(":");
    }

    static String[] getKeyValue(String keyValueArg) {
        String keyValueString = keyValueArg.substring(2);
        int pos = keyValueString.indexOf('=');
        if (pos >= 0) {
            return new String[] {keyValueString.substring(0, pos), keyValueString.substring(pos + 1)};
        }

        pos = keyValueString.indexOf(':');
        if (pos >= 0) {
            return new String[] {keyValueString.substring(0, pos), keyValueString.substring(pos + 1)};
        }

        throw new CliRuntimeException("no key value string: " + keyValueArg);
    }
}
