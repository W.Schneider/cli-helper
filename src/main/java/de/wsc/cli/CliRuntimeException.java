package de.wsc.cli;

public class CliRuntimeException extends RuntimeException {
    public CliRuntimeException(String message) {
        super(message);
    }
}
