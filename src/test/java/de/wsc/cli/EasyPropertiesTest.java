package de.wsc.cli;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EasyPropertiesTest {

    @Test
    void isKeyValue() {
        assertTrue(EasyProperties.isKeyValue("--hello:wolfgang"));
        assertTrue(EasyProperties.isKeyValue("--hello=wolfgang"));
    }

    @Test
    void getKeyValue() {
        List<String> args = EasyProperties.init("--hello:wolfgang", "--key=value", "-f", "test.txt");
        assertEquals(2, args.size());
        assertEquals("wolfgang", EasyProperties.getValue("hello"));
        assertEquals("value", EasyProperties.getValue("key"));

        EasyProperties.init("--hello: wolfgang schneider", "--key=value", "-f", "test.txt");
        assertEquals("wolfgang schneider", EasyProperties.getValue("hello"));

        assertEquals("bettina", EasyProperties.getValue("hallo"));

        EasyProperties.init("--hello= wolfgang=schneider", "--key=value", "-f", "test.txt");
        assertEquals("wolfgang=schneider", EasyProperties.getValue("hello"));

        EasyProperties.init("--easyfile=target/notsoeasy.properties");
        assertEquals("helene", EasyProperties.getValue("hallo"));
    }
}